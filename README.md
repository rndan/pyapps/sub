# MQTT Subcriber

# Dependencies
To use paho-mqtt 1.X install older version:\
pip3 install "paho-mqtt<2.0.0"

# Usage
python3 sub.py [broker] [topic]\
i.e.:\
python3 sub.py 127.0.0.1 in