# run with python3

import random
import time
import sys
from paho.mqtt import client as mqtt_client

# to use paho-mqtt 1.X install older version:
# pip3 install "paho-mqtt<2.0.0"
# to use with paho-mqtt 2.X change commented lines with new API  install it
# pip3 install paho-mqtt

# https://eclipse.dev/paho/files/paho.mqtt.python/html/migrations.html

# usage: 
# python3 sub.py [broker] [topic] 
# python3 sub.py 127.0.0.1 in

# Generate a Client ID with the subscriber prefix.
client_id = f'subscriber-{random.randint(0, 1000)}'

# default broker, port, topic
broker = 'broker.emqx.io'
port = 1883
topic = "in"
attemp = 1

if len(sys.argv) > 1:
  args = len(sys.argv) - 1

  pos = 1
  while (args >= pos):
    print ("Parameter at position %i is %s" % (pos, sys.argv[pos]))
    if pos == 1 :
      broker = sys.argv[pos]
    if pos  == 2 :
      topic =  sys.argv[pos]
    pos = pos + 1

print (f"MQTT broker: '{broker}'  topic '{topic}' deviceID '{client_id}'")

# username = 'emqx'
# password = 'public'

def on_message(client, userdata, msg):
    print(f"Received from topic '{msg.topic}':  {msg.payload.decode()}")

def subscribe(client: mqtt_client):
    client.subscribe(topic)
    client.on_message = on_message

def on_connect(client, userdata, flags, rc):
    global attemp
    if rc == 0:
      print(f"Connected or recoonected at attemp = {attemp} to MQTT Broker with result: {rc}")
      attemp = attemp + 1
      subscribe(client)
    else:
      print("Failed to connect to Broker, return code = ", rc)

def on_disconnect(client, userdata, rc):
    if rc != 0:
      print("Unexpected MQTT Broker disconnection!")

def connect_mqtt() -> mqtt_client:  
    # for older paho-mqtt version < 2.0
    client = mqtt_client.Client(client_id)
    # for paho-mqtt  version >= 2.X
    # client = mqtt_client.Client(mqtt_client.CallbackAPIVersion.VERSION1, client_id)

    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

if __name__ == '__main__':
  client = None
  try:
    client = connect_mqtt()
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    subscribe(client)
    try:
      client.loop_forever()
    except KeyboardInterrupt:
      print()
      #sys.exit(0)
  except:
      print("Cannot connect to " + broker + ":" +  str(port))
      sys.exit(1)
