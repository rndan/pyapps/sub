# Simple method for the visualization of the GPS 

# Install dependencies
python -m pip install numpy pandas matplotlib pillow

# Usage:
python3 vis.py [file.cvs] [file.json] [plot/save]\
where:\
file.cvs  - file with latitude, longitude\
file.json  - description of used map: path to map image and it corners coordinates\
i.e\
{"map" : "park.png", "corners" : {"ulat" : 0.0, "ulon" : 0.0, "dlat" : 1.1, "dlon" : 1.1} }\
plot/save - app action:\
save - to new image with gps track,\
plot - show on the screen, default value, can be ommited\
Example of commanad line to run app:\
python3 vis.py test.cvs test.json

# CSV file
It contains list of latituge, logitude pairs of gps track i.e. test.csv:\
49.5515, 25.6328\
49.5517, 25.6327\
49.5528, 25.6316

# How to for other locations
Go to interested place on www.openstreetmap.org\
In right side of page in menu select 'Share'\
Check box 'Set custom dimensions'\
Crop square of interest, and remeber it upper left and bottom right coordinates.\
Then 'Download'\
Using test.json template create and edit new json file with name of downloaded map image and it coordinates.




