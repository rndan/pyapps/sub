# run with python3

import json
import os
import math
import random
import redis
import time
import sys
from paho.mqtt import client as mqtt_client

# to use paho-mqtt 1.X install older version:
# pip3 install "paho-mqtt<2.0.0"
# to use with paho-mqtt 2.X change commented lines with new API  install it
# pip3 install paho-mqtt

# https://eclipse.dev/paho/files/paho.mqtt.python/html/migrations.html

# usage:
# python3 sub.py [broker] [topic]
# python3 sub.py 127.0.0.1 in

# Generate a Client ID with the subscriber prefix.
client_id = f'subscriber-{random.randint(0, 1000)}'

# default broker, port, topic
broker = 'broker.emqx.io'
port = 1883
topic = "in"
attemp = 1
rds = None
ts = None
dvs =  []
conf = "conf.cfg"

if len(sys.argv) > 1:
  args = len(sys.argv) - 1

  pos = 1
  while (args >= pos):
    print ("Parameter at position %i is %s" % (pos, sys.argv[pos]))
    if pos == 1 :
      broker = sys.argv[pos]
    if pos  == 2 :
      topic =  sys.argv[pos]
    pos = pos + 1

print (f"MQTT broker: '{broker}'  topic '{topic}' deviceID '{client_id}'")

fdir = os.path.dirname(sys.argv[0])

def configure():
  ds = []
  with open( os.path.join(fdir, '') + conf) as file:
    for line in file :
        if line and line != "" and line.strip() != "" :
            ds.append(line.strip())
    print(ds)
  return ds

# username = 'emqx'
# password = 'public'

def on_message(client, userdata, msg):
    pl = msg.payload.decode()
#    print(f"Received from topic '{msg.topic}': {pl}")
    jp = json.loads(pl)
    accf = None
    latf = None
    lonf = None
    global ts

    did = jp["deviceid"]
    sts = jp["ts"]       # sensor timestamp
    snr = jp["sensor"]
    dty = jp["type"]

    print(f"{sts} {did} {snr} {dty}")

#    if snr == "acc" and dty == "data" :
#        accf = float(jp["data"]["acc3d"])
#        print(f" {snr} {accf} ")

    if snr == "geo" and dty == "data" :
        #print(jp["data"]["lat"])
        lats = jp["data"]["lat"]
        lons = jp["data"]["lon"]
        #print(f"{lats} {lons}")
        latf = float(lats)
        lonf = float(lons)
        print(f"{latf} {lonf}")
        if abs(latf) > 90 :
          latf = latf / 100
        if abs(lonf) > 180 :
          lonf = lonf / 100

        #latf = round(latf, 6)
        #lonf = rouud(lonf, 6)
        print(f"{did} {snr} {sts} {latf} {lonf} ")

        idx = registered (did, dvs)
        if idx >= 0 :
          ts.add(dvs[idx]+":lat", sts, latf, labels={dvs[idx]:"gps"}, duplicate_policy="LAST", retention_msecs=5000)
          ts.add(dvs[idx]+":lon", sts, lonf, labels={dvs[idx]:"gps"}, duplicate_policy="LAST", retention_msecs=5000)
          print(f"{did} -> TS.{dvs[idx]}:lat {latf}  TS.{dvs[idx]}:lon {lonf} with label '{dvs[idx]}':gps")

def registered (device, devices):
    for i, d in enumerate(devices) :
        if d in device :
            return i
    return -1

def subscribe(client: mqtt_client):
    client.subscribe(topic)
    client.on_message = on_message

def on_connect(client, userdata, flags, rc):
    global attemp
    if rc == 0:
      print(f"Connected or recoonected at attemp = {attemp} to MQTT Broker with result: {rc}")
      attemp = attemp + 1
      subscribe(client)
    else:
      print("Failed to connect to Broker, return code = ", rc)

def on_disconnect(client, userdata, rc):
    if rc != 0:
      print("Unexpected MQTT Broker disconnection!")

def connect_mqtt() -> mqtt_client:
    # for older paho-mqtt version < 2.0
    client = mqtt_client.Client(client_id)
    # for paho-mqtt  version >= 2.X
    # client = mqtt_client.Client(mqtt_client.CallbackAPIVersion.VERSION1, client_id)

    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

if __name__ == '__main__':
  client = None
  rhost = "localhost"
  rport = 6379
  rds = redis.Redis(host=rhost, port=rport, db=0)
  ts = rds.ts()
  print(f"Ping redis {rhost}:{rport} {rds.ping()}")

  print("Configure timeseries with registered divices id:")
  dvs = configure()
  for dv in dvs :
    if dv =="":
      continue
    if not rds.exists(dv + ":lat") :
      ts.create(dv + ":lat", labels={dv:"gps"})
      print(f"Created TS for {dv}:lat with label {dv}=gps")
    if not rds.exists(dv + ":lon") :
      ts.create(dv + ":lon", labels={dv:"gps"})
      print(f"Created TS for {dv}:lon with label {dv}=gps")

  try:
    client = connect_mqtt()
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    subscribe(client)
    try:
      client.loop_forever()
    except KeyboardInterrupt:
      print()

  except:
      print("Cannot connect to " + broker + ":" +  str(port))
      sys.exit(1)
