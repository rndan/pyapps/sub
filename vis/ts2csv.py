# run with python3

import os
import math
import redis
import sys

conf = "conf.cfg"
devid = ""
lon_ofset = 0
lat_ofset = 0

if len(sys.argv) > 1:
  args = len(sys.argv) - 1

  pos = 1
  while (args >= pos):
    print ("Parameter at position %i is %s" % (pos, sys.argv[pos]))
    if pos == 1 :
      devid = sys.argv[pos]
    pos = pos + 1

fdir = os.path.dirname(sys.argv[0])

def toDegrees (inm) :
    frac, whole = math.modf(inm)
    return whole + 100.0 * frac/60.0

def configure():
  ds = []
  with open( os.path.join(fdir, '') + conf) as file:
    for line in file :
        if line and line != "" and line.strip() != "" :
            ds.append(line.strip())
    print(ds)
  return ds

if __name__ == '__main__':
  client = None
  rhost = "localhost"
  rport = 6379
  rds = redis.Redis(host=rhost, port=rport, db=0)
  ts = rds.ts()
  print(f"Ping redis {rhost}:{rport} {rds.ping()}")

  print("Read timeseries with registered divices id:")
  dvs = configure()
  for dv in dvs :
    llat = 0
    llon = 0
    lat = []
    lon = []
    plat = 0.0
    plon = 0.0

    if dv =="":
        continue
    if rds.exists(dv + ":lat") :
        print(f"Read TS for {dv}:lat with label {dv}=gps")
        lat = ts.range(dv + ":lat", "-", "+")
        llat = len(lat)
        if llat < 1 :
          print (llat)
          continue
    if rds.exists(dv + ":lon") :
        print(f"Read TS for {dv}:lon with label {dv}=gps")
        lon = ts.range(dv + ":lon", "-", "+")
        llon = len(lon)
        if llon < 1 :
          print (llon)
          continue

    with open(dv.replace(':','_') + '.csv', 'w') as wf:
      for i in range(0,llat) :
        # skip repeated TS entries
        if plat != lat[i][1] and plon != lon[i][1] :
          # convert coordinates fractial part from minutes to degrees for visualisation on a map
          latd = round(toDegrees(lat[i][1] - lat_ofset),7)
          lond = round(toDegrees(lon[i][1] -lon_ofset),7)
          print (f"{latd}, {lond}")
          wf.write(f"{latd}, {lond} \n")

        plat = lat[i][1]
        plon = lon[i][1]

  sys.exit(1)
