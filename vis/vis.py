import sys
import json
from gps_class import GPSVis

# Usage:
# python3 vis.py [file.cvs] [map.json] [plot/save]
# where:
# file.cvs  - file with latitude, longitude
# map.json  - description of used map: path to map image and it corners coordinates
# i.e
# {"map" : "park.png", "corners" : {"ulat" : 0.0, "ulon" : 0.0, "dlat" : 1.1, "dlon" : 1.1} }
# plot/save - app action:
# save - to new image with gps track,
# plot - show on the screen

# defaults
mapf="test.json"
cvsf="test.csv"
act="plot"

args = len(sys.argv) - 1

pos = 1
while (args >= pos):
    print ("Parameter at position %i is %s" % (pos, sys.argv[pos]))
    if pos == 1 :
      cvsf = sys.argv[pos]
    if pos  == 2 :
      mapf = sys.argv[pos]
    if pos  == 3 :
      a = sys.argv[pos]
      if a == "save" :
        act = "save"
    pos = pos + 1

f = open(mapf)
jmap = json.load(f)

map2 = jmap ["map"]
ux = jmap ["corners"] ["ulat"]
uy = jmap ["corners"] ["ulon"]
bx = jmap ["corners"] ["blat"]
by = jmap ["corners"] ["blon"]

print (f"Visualize data from {cvsf} using {map2} with corners {ux},{uy} {bx},{by}")

vis = GPSVis(
             data_path=cvsf, # gps lat/lon file
             map_path=map2,  # Path to map downloaded from the OSM.
             points= ( ux, uy, bx, by)) # map corners top/bottom

vis.create_image(color=(0, 0, 255), width=3)  # Set the color and the width of the GNSS tracks.
vis.plot_map(output=act)

print()
